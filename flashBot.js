var fs = require('fs');
var path = require('path');
const Discord = require('discord.js');
const Bot = new Discord.Client();
const auth = require('./auth.json');
const token = auth.token;
Bot.login(token);

var NotifyChannel;

var express = require('express');
//var request = require('request');
var app = express();
const bodyParser = require('body-parser');

let reqCount = 0;

app.set('trust proxy', true);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json('application/json'));

//activate bot
Bot.on('ready', () => {
	console.log(`[Start] ${new Date()}`);
	//init variables for channels
	NotifyChannel = Bot.channels.find('id', '512373234231148586');
});

//bitbucket
//http: //108.61.78.227:5000/webhook
app.post('/webhook', function(req, res) {
	const repoName = req.body.repository.name;
	const repoImage = req.body.repository.links.avatar.href;
	const authorName = req.body.actor.display_name;
	const authorImage = req.body.actor.links.avatar.href;

	// Loops through all webhook changes
	req.body.push.changes.forEach(change => {
		const embedColor = getNextColor(reqCount++);
		//const embedColor = Math.floor(Math.random() * 0xFFFFFF);
		const forcedPrefix = change.forced ? '!' : '';

		// branch was deleted
		if (change.closed) {
			const embed = new Discord.RichEmbed()
				.setColor(embedColor)
				.setAuthor(`${authorName} closed a remote branch`, authorImage)
				.setTitle(`${repoName}/${change.old.name}`)
				.setTimestamp(new Date());
			NotifyChannel.send(embed);
			return;
		}

		// branch was created
		if (change.created) {
			const titleString = `New branch '${change.new.name}' was pushed`;
			const embed = new Discord.RichEmbed()
				.setColor(embedColor)
				.setAuthor(`${authorName} pushed a new branch`, authorImage)
				.setTitle(`${repoName}/${change.new.name}`)
				.setURL(change.new.links.html.href)
				.setTimestamp(new Date());
			NotifyChannel.send(embed);
			return;
		}

		const overviewURL = change.links.html.href;
		const footerString = `To branch '${change.new.name}'`;

		// property truncated means more than 5 commits were received
		if (change.truncated) {
			const embed = new Discord.RichEmbed()
				.setColor(embedColor)
				.setTitle('...and more')
				.setURL(overviewURL);
			NotifyChannel.send(embed);
		}

		// Loops through all received commits (5 max)
		change.commits.reverse().forEach(commit => {
			const authorString = `${commit.author.user.display_name} pushed a new commit`;
			const embed = new Discord.RichEmbed()
				.setColor(embedColor)
				.setAuthor(authorString, commit.author.user.links.avatar.href, overviewURL)
				.setTitle(forcedPrefix + commit.hash)
				.setURL(commit.links.html.href)
				.setThumbnail(repoImage)
				.setTimestamp(commit.date)
				.setDescription(commit.message)
				.setFooter(footerString);
			NotifyChannel.send(embed);
		});
	});

	res.send({ message: 'we received webhook' });
});

app.listen(5500, () => console.log('Now listening to 5500'));

//Reaction handler
Bot.on('messageReactionAdd', (reaction, user) => {
	//console.log(reaction)
	var yourChannel = Bot.channels.find('id', reaction.message.channel.id);
	var reactionAuthor;
	for (user of reaction.users) {
		reactionAuthor = user[1].username;
	}
	yourChannel.send(reactionAuthor + ' reacted to ' + reaction.message.author + ' with ' + reaction._emoji.name);
});

let prefix = '!';
Bot.on('message', message => {
	if (message.content.startsWith(prefix)) {
		var cmd = message.content;

		switch (cmd) {
			case '!roll':
				message.channel.send(
					message.author.username + ' has rolled ' + (Math.floor(Math.random() * 100) + 1) + '!'
				);
				break;
			case '!cookies':
				message.channel.send(':cookie: :cookie: :cookie: :cookie: :cookie:');
				break;
			case '!kaggle':
				message.channel.send('https://www.kaggle.com/c/ift3395-6390-f2018');
				break;
			case '!trello':
				message.channel.send('https://trello.com/b/q4dDKW8i/kaggle-babble');
				break;
			case '!coffee':
				message.channel.send(':coffee: :coffee: :coffee: :coffee: :coffee: ');
				break;
			case '!rainbow':
				for (let i = 0; i < 12; i++) {
					const clr = getNextColor(reqCount++);
					message.channel.send(new Discord.RichEmbed().setTitle(i + '::#' + clr.toString(16)).setColor(clr));
				}
				break;
		}
	}
});

function getNextColor(int) {
	const CYCLE = 12; // Number of colors before the cycle repeats
	const SATURATION = [64, 64, 64]; // Hue; 0 = greyscale, 127 = high saturation
	const BRIGHTNESS = [191, 191, 191]; // Brightness; 0 = dark, 255 = bright

	const r = Math.floor(Math.sin((Math.PI / CYCLE) * 2 * int) * SATURATION[0]) + BRIGHTNESS[0];
	const g = Math.floor(Math.sin((Math.PI / CYCLE) * 2 * int + (Math.PI * 2) / 3) * SATURATION[1]) + BRIGHTNESS[1];
	const b = Math.floor(Math.sin((Math.PI / CYCLE) * 2 * int + (Math.PI * 4) / 3) * SATURATION[2]) + BRIGHTNESS[2];
	return r * 65536 + g * 256 + b;
}
